Mastodon installation guide for uberspace
================================================
## Introduction
With the plans of Elon Musk buying Twitter in the year 2022 some people are looking for alternatives.  
One such alternative often mentioned by the press is Mastodon. Mastodon works almost exactly like twitter with the huge
exception that it is a federated service. This means that everybody can install their own instance and keep ownership
of everything created in that instance but also all instances are interconnected and are able to communicate with one another.
This concept got named the Fediverse and connects much more services than mastodon with a common protocol named ActivityPub.
As a web developer I was really curious how this works in practice, and I was eager to set up my own instance.
I decided to use uberspace for hosting as it is my number 1 hoster for "tinkering" projects. Unfortunately mastodon
is strongly services oriented which is good from an architecture perspective but hard to operate outside of cloud providers
which can get expensive quite fast.

So I sat down and tried to work around the limitations and brought [social.nerdcrisis.de](https://social.nerdcrisis.de/)
to live. My own mastodon instance running on uberspace hosting. Afterwards I condensed my knowledge and experiences down
into this little guide. If it was helpful to you, or you have any questions, just come over to my mastodon and say hi. 

## Files
The `etc/services.d/mastodon.ini` and `nginx/conf/nginx.conf` are templates for two of the files described in the guide.
Please follow the guide and see those files as reference.

## Guide
The whole installation and configuration process is described [here](doc/install.md).