Installing your own Mastodon server on uberspace
================================================

## Sign up for uberspace
The fact that you are reading this tells me that you most certainly already know about [uberspace](https://uberspace.de/).
When installing Mastodon I recommend you to create a new dedicated uberspace just for your Mastodon instance as it takes
up quite some space and resources. You should also consider adding mor storage to your uberspace as mastodon is caching
a lot of media files even if they are initially hosted on other servers. We will set up a cleanup routine at the end of
the tutorial, but I would really recommend around 30gb of storage.

## Set up postgresql and redis
For your installation to work properly you need to have a postgres database and a redis cache running.
The uberspace lab does a great job explaining how to set both of them up.
 - Refer to https://lab.uberspace.de/guide_redis/ for the redis install
 - Refer to https://lab.uberspace.de/guide_postgresql/ for the postgres install
 
## Check out the mastodon source and build the application
You can check out the latest release of mastodon by executing the following:
```bash
git clone https://github.com/tootsuite/mastodon.git mastodon && cd mastodon
git checkout $(git tag -l | grep -v 'rc[0-9]*$' | sort -V | tail -n 1)
```
Next you need to install Ruby and JavaScript dependencies
```bash
bundle config deployment 'true'
bundle config without 'development test'
bundle install -j$(getconf _NPROCESSORS_ONLN)
yarn install --pure-lockfile
```
The next step according to the [official documentation](https://docs.joinmastodon.org/admin/install/) would be to
execute the setup wizard. We should do that to get the basic structure for our `.env.production` file and let it 
generate some secrets, but be aware that some steps will fail on uberspace. When the wizard asks for your redis host
and port just leave them empty we need to change `.env.production` later to make use of the socket based connection to
redis. When the wizard tries to pre-build assets this will most likely fail because it consumes too many RAM.
We will have to work around that later on.
```bash
RAILS_ENV=production bundle exec rake mastodon:setup
```

### Fix environment file
Open `.env.production` in the editor of your liking.
As already mentioned above you need to change the redis configuration. Just remove the `REDIS_HOST` and `REDIS_HOST`
variables entirely and replace them with `REDIS_URL=unix:///home/<username>/.redis/sock` where `<username>`
:is your uberspace account name.

### Pre-build assets on a different machine
Because of the high RAM usage of the `assets:precompile` command we need to execute it elsewhere. I recommend doing it
within a local linux installation making use of docker. First you need to download your installation to the machine.
Please be aware that you need to replace `<username>` and `<server>` in all following commands with your uberspace username
and the name of the server your uberspace is hosted at respectively.
```bash
rsync -trzv --delete <username>@<server>.uberspace.de:/home/<username>/mastodon/ mastodon
```
Then you can build the assets. This step will take quite a while as it will not just build the assets but also
download and build the docker images beforehand.
```bash
cd mastodon
docker-compose pull && docker-compose build && docker-compose run --rm web rails assets:precompile
```
Lastly you just need to upload your compiled assets:
```bash
cd ..
rsync -trzv ./mastodon/public <username>@<server>.uberspace.de:/home/<username>/mastodon/
```

### Create admin user
Because the setup wizard died while trying to compile assets, it did not create an admin user for you.
Luckily mastodon comes with a set of very versatile cli tools so that you can just create the admin user with them.
Please replace `<mastodon-user>` with the username you want to use as your mastodon account and `<email>` with
your own email address.
```bash
RAILS_ENV=production bin/tootctl accounts create <mastodon-user> --email <email> --confirmed --role admin
```

Custom nginx installation
-------------------------
Your mastodon instance consists of several services and some of them need to be accessible via the same domain.
While the web backends of uberspace7 are very versatile and do a great job when connecting ***different*** services to 
***different*** domains, we need to setup our own routing when it comes to connecting services to the ***same*** domain.  
For this task we will use nginx.

### install your own nginx
Download the latest mainline release from [nginx download page](http://nginx.org/en/download.html) and extract it:
```bash
wget http://nginx.org/download/nginx-1.21.6.tar.gz && tar xf nginx-1.21.6.tar.gz
```
Configure the installation to be installed into your home and compile it:
```bash
cd nginx-1.21.6
./configure --prefix=$HOME/nginx --with-http_realip_module 
make
make install
```
It is recommended to set up a symlink for easy access to the nginx binary. An alternative would be to add 
`~/nginx/sbin` to your `PATH`.
```bash
ln -s ~/nginx/sbin/nginx ~/bin
```
Make a new folder to use as cache storage later on:
```bash
mkdir -p ~/cache/nginx
```

Clean up:
```bash
cd ~ && rm -rf nginx-1.21.6 && rm nginx-1.21.6.tar.gz
```

### configure nginx for mastodon
Open the file `~/nginx/conf/nginx.conf` and replace the whole `server` section with the last `server` section from 
`~/mastodon/dist/nginx.conf`. Also copy the following section from the file:
```
    map $http_upgrade $connection_upgrade {
      default upgrade;
      ''      close;
    }
```
You need to make some changes because of the way uberspace operates. If you want to get a better understanding of why 
this is the way it is, I recommend reading [the article in the manual](https://manual.uberspace.de/background-http-stack/).

Add the `proxy_cache_path` in front of the new `server` section:
```nginx
...
proxy_cache_path /home/<username>/cache/nginx levels=1:2 keys_zone=CACHE:10m inactive=7d max_size=1g;

server {
...
```
And be sure to replace `<username>` with your uberspace username.

Now you need to change the `server` section. We don't need all the ssl configuration as our installation is
behind the uberspace nginx and uberspace is handling encryption very well and automatically.

Therefore, you delete every `ssl_*` directive. You also replace the variable `$scheme` with the string `'https'` as
uberspace makes sure that you are always on ssl when communicating with the outside world. This occurs at two postions.

You now need a port for our custom nginx.
According to [uberspace manual](https://manual.uberspace.de/web-backends/) everything between 1024 and 65535 is fine.
3000 and 4000 is occupied by the streaming and backend services of mastodon, so I choose 8080. You need to change the 
two `listen` directives accordingly.

Your new `server` section should look like this:
```
    ...
    server {
      listen 8080;
      listen [::]:8080;
      server_name <domain>;

      keepalive_timeout    70;
      sendfile             on;
      client_max_body_size 80m;

      root /home/<username>/live/public;

      gzip on;
      ...
```
Be sure to replace `<username>` with your uberspace user and `<domain>` with the domain you are going to use.

Find your uberspace internal IP Address by executing `ip addr` look for the address starting with 100.64. and note it down.
You need this IP to replace both `proxy_pass` directives of the services now:
```

        proxy_pass http://<ip>:3000;
        
        ...
        
        proxy_pass http://<ip>:4000;
```

Bringing everything together
----------------------------
Before you can introduce your mastodon server to the world you need to start up all the services.
Because you don't want to do that manually you should set up everything as deamons.
### Set up supervisor
You need to create a .ini file for supervisor to start all your components. For ease of use I grouped everything together.
First you need to create the file `~/etc/services.d/mastodon.ini` and then you need to copy the contents:
```ini
[group:mastodon]
programs=server,backend,sidekiq,streaming

[program:server]
command=nginx -g "daemon off;"
stdout_logfile=%(ENV_HOME)s/mastodon/log/nginx.log
stdout_logfile_maxbytes=1MB
stdout_logfile_backups=10
stopasgroup=true

[program:backend]
command=bundle exec puma -C config/puma.rb
directory=%(ENV_HOME)s/mastodon
stdout_logfile=%(ENV_HOME)s/mastodon/log/puma.log
stdout_logfile_maxbytes=1MB
stdout_logfile_backups=10
redirect_stderr=true
environment=RAILS_ENV="production",PORT=3000,BIND="0.0.0.0"
stopasgroup=true

[program:sidekiq]
command=bundle exec sidekiq -c 5 -q default -q mailers -q pull -q push -q scheduler
directory=%(ENV_HOME)s/mastodon
stdout_logfile=%(ENV_HOME)s/mastodon/log/sidekiq.log
stdout_logfile_maxbytes=1MB
stdout_logfile_backups=10
redirect_stderr=true
environment=RAILS_ENV="production",DB_POOL=5
stopasgroup=true

[program:streaming]
command=/usr/bin/npm run start
directory=%(ENV_HOME)s/mastodon
stdout_logfile=%(ENV_HOME)s/mastodon/log/streaming.log
stdout_logfile_maxbytes=1MB
stdout_logfile_backups=10
redirect_stderr=true
environment=PORT=4000,BIND="0.0.0.0"
stopasgroup=true
```
Now you let supervisor read the file and run everything by executing:
```bash
supervisorctl reread
supervisorctl update
```

### Configuring ubersapce web backend
The last step before accessing your new mastodon installation is to connect your nginx with the nginx of uberspace.
This can easily be done by using the web backend feature of your uberspace.
```bash
uberspace web backend set <domain> --http --port 8080
```
## Add Cronjobs for housekeeping
There are two commands for cleaning up your instance and remove old cached media. I highly recommend adding those to your
cronjobs or otherwise the storage space used will expand pretty fast.
Here are both commands as they need to be added to the crontab:
```crontab
@weekly RAILS_ENV=production ~/mastodon/bin/tootctl media remove --days=7
@weekly RAILS_ENV=production ~/mastodon/bin/tootctl preview_cards remove --days=7
```
